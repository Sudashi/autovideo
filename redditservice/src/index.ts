/**
 * @author Sascha Lorenz <gitlab.com/Sudashi> <github.com/Sudashiii>
 * This Service pull videos to rate from reddit and saves them to the database
 */
import snoowrap from 'snoowrap';
import express from 'express';
import { footage } from './interface/types';
import api from '../config/redditApiKey.js'
import _ from "lodash";
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();
const app = express();
const PORT = 3002;
const r = new snoowrap(api);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Cache
let cache = [];
let cacheTarget = 6 * 60;
let cacheRestockTrigger = 2 * 60;
let restocking = false;

// General
let subreddits = ['dankvideos', 'hmm']
let emittedFootage = []; // Store posts that have been requested, but not yet used, as to avoid sending duplicates if multiple clients request videos

async function getFootage(minimumDuration = 1, subName: string, usedFootage: footage[]): Promise<footage[]> {

  let posts: footage[] = [];
  let postLengthSec: number = 0;

  let subreddit = {
    name: subName,
    afterId: "",
  };

  async function scrapeLoop(): Promise<footage[]> {

    let res = await r.getTop(subreddit.name, {
      time: "month",
      limit: 10,
      after: subreddit.afterId,
      count: 555
    });

    res.forEach((post) => {
      if (post.is_video && post.media.reddit_video.duration <= 30 && postLengthSec < minimumDuration) {

        // Duplicate checks
        if (usedFootage.some((e) => e.title === post.title)) {
          // Duplicate from old session
          console.log("Duplicate from old session, aborting...");
          return;
        }
        if (posts.some((e) => e.title === post.title)) {
          // Duplicate from current session
          console.log("Duplicate from current session, aborting...");
          return;
        }

        posts.push({
          url: post.url,
          title: post.title,
          timestamp: new Date(),
          duration: Math.floor(post.media.reddit_video.duration),
          size: 0,
          used: false
        });

        postLengthSec += post.media.reddit_video.duration;
        console.log("Got " + post.title + " from " + subreddit.name);
        // console.log('Current length: ' + postLengthSec);
      }
    });

    if (res.length) {
      // Otherwhise update the pagination
      subreddit.afterId = "t3_" + res[res.length - 1].id;
    } else {
       // Switch subreddit when res is zero
       subreddit.name = _.sample(subreddits);
    }

    if (postLengthSec < minimumDuration) {
      await scrapeLoop();
    } else {
      // Otherwhise, once there is enough content, start fetching video/audio, combine and save them
      console.log("Got " + posts.length + " posts!");
      return posts;
    }

  }

  await scrapeLoop();
  return posts;
}

async function loadUsedFootage(): Promise<footage[]> {
  const posts = await prisma.footage.findMany();
  console.log('Successfully loaded used footage!')
  return posts || [];
}

async function updateServiceData(posts: footage[]): Promise<void> {
  if (posts.length) {
    await prisma.footage.createMany({
      data: posts,
    }).catch(e => console.log(e));
    console.log("Inserted to db!");
  }
}

function totalDuration(footage: footage[]): number {
  let duration = 0;
  footage.forEach(footage => {
    duration += footage.duration
  });
  return duration;
}

function getCachedFootage(minimumDuration: number): footage[] {

  let duration = 0;
  let footageArr = [];

  for (let i = 0; i < cache.length; i++) {

    const footage = cache[i];
    footageArr.push(footage);
    duration += footage.duration;

    if (duration >= minimumDuration) {
      break;
    }
  }

  // Remove footageArr from cache
  cache = cache.filter(footage => !footageArr.includes(footage));

  return footageArr;
}

app.get('/redditService/getSpecificFootage/:subreddit/:minimumDuration?', async (req, res) => {
  console.log("---------------------------");
  let minimumDuration = Number(req.params.minimumDuration) ?? 1;
  let subreddit = req.params.subreddit;

  console.log(`Received footage request at ${new Date().toLocaleString("en-GB")} of ${minimumDuration}s, started scraper...`);
  let posts = await getFootage(minimumDuration, subreddit, emittedFootage);
  emittedFootage = [...emittedFootage, ...posts];

  res.status(200).json(posts);
});

app.get('/redditService/getFootage/:minimumDuration?', async (req, res) => {

  // Footage section
  let minimumDuration = Number(req.params.minimumDuration) ?? 1;
  console.log("---------------------------");
  console.log(`Received footage request at ${new Date().toLocaleString("en-GB")} of ${minimumDuration}s...`);

  let footage = getCachedFootage(minimumDuration);
  emittedFootage = [...emittedFootage, ...footage];
  res.status(200).json(footage);

  // If cache is below restockTrigger, restock cache up to cacheTarget
  let cacheDuration = totalDuration(cache);

  if (cacheDuration < cacheRestockTrigger && !restocking) {
    restocking = true;

    let durationNeeded = cacheTarget - cacheDuration;
    console.log("Restocking cache (" + durationNeeded + "s)...");
    let additionalFootage: footage[] = await getFootage(durationNeeded, _.sample(subreddits), emittedFootage);

    cache = [...cache, ...additionalFootage];
    restocking = false;
  }

});

app.post('/redditService/setFootageUsed', async (req, res) => {
  let footage: footage[] = req.body.footage;

  console.log('Trying to insert: ', footage)
  updateServiceData(footage);
  res.status(200).json("Updated footage!");
});

app.get("/redditService", (req, res) => {
  res.status(200).json({
    status: "RedditService online ✅",
    cacheDuration: totalDuration(cache),
    cache,
    emittedDuration: totalDuration(emittedFootage),
    emittedFootage
  });
});

(async () => {

  // Emitted footage
  emittedFootage = await loadUsedFootage();

  // Cache 6 mins of footage
  console.log("Caching " + cacheTarget / 60 + " minutes of footage...");
  let footage = await getFootage(cacheTarget / 2, _.sample(subreddits), emittedFootage);
  let footage2 = await getFootage(cacheTarget / 2, _.sample(subreddits), emittedFootage);
  cache = [...footage, ...footage2];

  // Server
  app.listen(PORT, () => console.log(`Reddit-Service running on port ${PORT}`));

})();