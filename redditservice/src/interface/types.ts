interface footage {
    id?: number;
    url: string;
    title: string;
    timestamp: Date;
    duration: number;
    size: number;
    used: boolean;
}

export {
    footage
}