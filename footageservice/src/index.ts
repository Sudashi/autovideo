import { PrismaClient } from '@prisma/client'
import express from 'express';
import { footage } from './interface/types';

const prisma = new PrismaClient();

// Express

const PORT = 3004;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * Returns as much footage as the client requests with minimumDuration from the database
 */
app.get("/footageService/getFootage/:minimumDuration?", async (req, res) => {
    let minimumDuration: number = Number(req.params.minimumDuration) ?? 1;
    try {

        // Solution from https://stackoverflow.com/a/62049544/10233170
        let validatedFootage: footage[] = await prisma.$queryRaw`
            SELECT id, url, title, timestamp, duration, size, used
                FROM (
                SELECT *, SUM(duration) OVER (ORDER BY timestamp ASC) - duration as total_duration 
                FROM validatedfootage
                WHERE used = 'false'
                ) AS o 
            WHERE o.total_duration < ${minimumDuration}
        `;

        // If the requested duration is smaller than the smallest total_duration entry of the above query, nothing is returned
        // So in those cases, we simply return a single random unsued footage
        if (validatedFootage.length == 0) {
            validatedFootage = [await prisma.validatedfootage.findFirst({
                where: {
                    used: false
                },
            })];
        }

        console.log("Queried footage from database!");
        res.status(200).json(validatedFootage);

    } catch (err) {
        console.error("Couldn't query footage from database...", err);
        res.sendStatus(500);
    }
});

/**
 * Sets the used status to true for the given footage in the database
 */
app.post("/footageService/setFootageUsed", async (req, res) => {
    let footage: footage[] = req.body.footage;

    try {
        if (footage?.length > 0) {

            let idArray = footage.map(footage => footage.id);
            await prisma.validatedfootage.updateMany({
                where: {
                    id: {
                        in: idArray,
                    },
                },
                data: {
                    used: true
                },
            });
            console.log("Updated footage in database!");
            res.sendStatus(200);

        } else {
            res.status(400, "Footage array not set!");
        }
    } catch (err) {
        console.error(`Couldn't update footage in database...`, err);
        res.sendStatus(500);
    }
});

/**
 * Insert validated footage into the database
 */
app.post("/footageService/addFootage", async (req, res) => {
    let footage: footage = req.body.footage;
    console.log('Trying to insert: ', footage);

    try {
        if (footage) {
            await prisma.validatedfootage.create({
                data: footage,
            });
            console.log("Inserted footage in database!");
            res.sendStatus(200);
        } else {
            res.status(400, "Footage object not set!");
        }
    } catch (err) {
        console.error(`Couldn't insert footage in database...`, err);
        res.sendStatus(500);
    }
});

app.get("/footageService", async (req, res) => {

    const unusedFootage = await prisma.validatedfootage.aggregate({
        where: {
            used: false
        },
        _count: {
            url: true,
        },
        _sum: {
            duration: true
        }
    })

    const usedFootage = await prisma.validatedfootage.aggregate({
        where: {
            used: true
        },
        _count: {
            url: true,
        },
        _sum: {
            duration: true
        }
    })

    res.status(200).json({
        status: "FootageService online ✅",
        unusedFootage: unusedFootage._count.url,
        unusedFootageDuration: unusedFootage._sum.duration,
        usedFootage: usedFootage._count.url,
        usedFootageDuration: usedFootage._sum.duration
    });

});

app.listen(PORT, () => console.log(`FootageService running on port ${PORT}`));