interface footage {
  id?: number;
  url: string;
  title: string;
  timestamp: Date;
  size: number;
  duration: number;
  used: boolean;
}

export { footage };
