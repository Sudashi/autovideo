import axios from "axios";
import fs from "fs-extra";

// TODO: DOCU

async function getRandomThumbnail(workingDir: string): Promise<{ id: number, sourceurl: string }> {
  try {
    // Get random thumbnail from thumbnailService
    const { data } = await axios.get("http://localhost:3000/thumbnailService2/getThumbnail");
    const thumbnail = data.thumbnail;
 
    // Convert base64 string to img and save it to file system
    await fs.writeFile(`${workingDir}/tempThumbnail.png`, thumbnail.file, { encoding: 'base64' });

    return { id: thumbnail.id, sourceurl: thumbnail.sourceurl };

  } catch (err) {
    console.log(err)
    console.log("Error requesting thumbnail from thumbnailService");
    return { id: null, sourceurl: "69" };
  }
}

async function setThumbnailUsed(thumbId: number): Promise<void> {
  try {
    await axios.post("http://localhost:3000/thumbnailService2/setThumbnailUsed", { id: thumbId });
    console.log("Successfully told thumbnailService that thumbnail has been used!");
  } catch (error) {
    console.error("Couldn't run setThumbnailUsed, there might be an error with the thumbnailService! Please update manually!", error);
  }
}

export { getRandomThumbnail, setThumbnailUsed };
