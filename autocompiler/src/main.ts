import axios from "axios";
import spawn from "await-spawn";
import editFootage from "./editor.js";
import * as yt from "./apilessyt";
import fs from "fs-extra";
import { getRandomThumbnail, setThumbnailUsed } from "./thumbnails.js";
import path from "path";
import { footage, servicePorts, services } from "./interface/types.js";
import dateFormat from "dateformat";
import logger from "node-color-log";
import _ from 'lodash'
import { platform } from "os";
import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

const { DISCORD_SERVICE, REDDIT_SERVICE, TIKTOK_SERVICE } = services;
let videoDir = path.join(`${__dirname}/../videos`);

export async function createVideo(duration: number): Promise<void> {
  log("Started compilation!", true);

  // Retrieve video links from service bots
  let footage = await requestServiceFootage(duration);
  log("Successfully got footage links from all services!");

  // Download links
  await downloadFootage(footage, videoDir);

  // Edit videos and turn them into one single video
  let compilationPath = await editFootage(videoDir);

  // Upload to youtube (make sure to run googleLogin.js first)

  await uploadCompilation({
    channelId: "UCz0RxV11PdhM-O0vRYoSLIA",
    // channelId: "UC-Ya4w-_IqS258QvAhSqxiQ",
    video: compilationPath,
    pageOne: {
      // Title can be max 100 chars long!
      title: duration > 1 ? await generateCompTitle() : 'Meme Status',
      description:
        "💖 Thank you for watching 💖\n\nIf you enjoy what you are seeing feel free to subscribe and hit the bell 🔔!\nWrite down in the comments what you think about the Video👉👈!\n\nIf you want a clip of yours removed or want to contact me for any other reason don't hesitate to send me an email.\n👇👇👇👇\nsjohnny1690@gmail.com\n👆👆👆👆\n\nwhy awe you stiww weading the descwiption owo? what awe you doing down hewe uwu? if you have wead this faw pwease shawe this video with youw fwiends🥺👉👈\n\ntags:\n#memes #videos #tiktiok\n\nmemes, meme, friday night funkin memes, fnf memes, bernie sanders memes, grubhub memes, perfectly cut memes, dankest, tiktok Memes, minecraft memes, cute animals memes, spongebob Memes, dank memes, try not to laugh, dank compilation, memes, funny memes, pewdiepie, tiktok, big chungus, freememeskids, unususal videos, meme vine, dank meme compilation, best memes, rip vine, unusual videos compilation, fortnite memes, dank memes compilation, grandayy, pewdiepie vs tseries, airpods, dank, fresh memes, filthy frank, hefty, roblox memes, yoda memes, gta memes, youtube rewind memes, weird memes, belle delphine, rick rolled, memecorp, lego city memes, best memes compilation, joker memes, callmecarson, wide putin, funny cat memes, Ugh, fine, I guess you are my little Pogchamp, spiderman memes, shaq memes,, boom beach memes, monke memes, mario memes, cj memes, breaking bad walter memes, cyberbug 2077 memes, bingus memes, wake up omegle memes, jojo's bizzare adventure memes, gordon ramsay memes, tyler1 memes, spiderman memes, aunt cass memes, mr bean memes, dababy memes,  rick and morty memes, patrick memes, amogus memes, dog memes, emisoccer memes, deadpool memes, aunt cass memes, friday night funkin memes, dababy memes, mr bean memes, mario memes, peter griffin memes, south park memes, no god pls no meme, quagmire memes, twomad memes, edp445 memes, trade offer memes, don't flirt with him memes, lightning mcqueen, joe biden memes, dank doodle memes, dean norris memes",
      thumb: duration > 1 ? null : `${videoDir}/tempThumbnail.png`,
    },
    pageTwo: {
      // cards: {
      //   enabled: true,
      //   type: yt.types.card.PLAYLIST,
      //   name: "PLB-Sd_X9jxPoLcnk6-3WihvS10XZpT7jJ",
      //   anyVideo: true,
      // },
      endcard: {
        enabled: duration > 1 ? true : false,
        import: true,
        importId: '98_9WDNnfjw'
      }
    },
    pageFour: {
      visibility: duration > 1 ? yt.types.visibility.PRIVATE : yt.types.visibility.PUBLIC
    },
    creatorComment: {
      text: "source at bottom of description\njoin discord: https://discord.gg/kAFf4agIvK\nalso don't forget to like the video :]",
      pin: true,
    },
  });

  // Clear videoDir
  fs.emptyDirSync(videoDir);
  log("Cleared video directory!");

  // Update the validated footage in the database
  await setValFootageToUsed(footage);

  // Increase version number by one for compilations
  if (duration > 1) {
    await increaseVersionNumber();
  }

  log("Done!");

}

async function uploadCompilation(videoSpecs: yt.types.uploadSpecs): Promise<void> {
  // try {
  let thumbObj: any;

  if (videoSpecs.pageOne.thumb !== null) {
    // Thumbnail
    log("Downloading thumbnail....");
    thumbObj = await getRandomThumbnail(videoDir);

    // Peak code quality
    if (thumbObj.sourceurl === "69") {
      // Do nothing
    } else if (thumbObj.sourceurl) {
      log("Finished downloading thumbnail!");
      let descsource = "redditcom" + thumbObj.sourceurl.split(".com")[1];
      videoSpecs.pageOne.description += "\n\nsource: " + descsource;
    } else {
      log("Wasn't able to get thumbnail from thumbnailService!");
      videoSpecs.pageOne.description += "\n\nsource: " + "i sadly lost the link but its somewhere in discord";
    }
  }

  // Upload via apilessyt
  log("Uploading video...");
  yt.setHead(false);
  await yt.upload(videoSpecs);
  log("Uploaded video successfully!");

  if (videoSpecs.pageOne.thumb !== null) {
    if (thumbObj.sourceurl) {
      // Tell thumbnail service that thumb has been used successfully
      await setThumbnailUsed(thumbObj.id);
      log(`thumbsourceUrl: ${thumbObj.sourceurl}`);

      // Delete used thumb
      try {
        fs.unlinkSync(`${videoDir}/tempThumbnail.png`);
        log("Deleted tempThumbnail!");
      } catch (err) {
        error("Couldn't delete tempThumbnail!", err);
      }
    }
  }
}

/**
 * Downloads the provided links into a target directory. Deletes the contents of the target directory before downloading.
 *
 * @param {footage[]} links - The links to the footage to download
 * @param {String} targetDir - Defines the directory the videos should be downloaded to
 * @returns A string array of the successfully downloaded links only
 */
async function downloadFootage(footage: footage[], targetDir: string): Promise<void> {

  // Clear download directory
  fs.emptyDirSync(targetDir);
  log("Cleared download directory!");

  // Shuffle links
  footage = _.shuffle(footage);

  // Download videos
  log(`Starting download of ${footage.length} videos...`);

  for (let i = 0; i < footage.length; i++) {
    const link = footage[i].url;
    try {
      await spawn("youtube-dl", [link, "-o", `${targetDir}/${i}.mp4`]);
      log(`Downloaded ${link}`);
    } catch (err) {
      // Set footage to used on error
      await setValFootageToUsed([ footage[i] ]);
      error(`There was an error downloading ${link}!`, err.stderr._bufs.toString());
    }
  }
  log("Finished video downloads!");
}

async function requestServiceFootage(duration: number): Promise<footage[]> {
  try {

    log(`Requesting ${duration}s of footage from footageService. Waiting for server response...`);
    let res = await axios.get(`http://localhost:3004/footageService/getFootage/${duration}`);
    let validatedFootage: footage[] = res.data;

    log("Got footage from footageService! Got " + validatedFootage.length + " footage objects.");
    return validatedFootage;

  } catch (err) {
    error("Couldn't get footage from footageService!", err);
  }
}

async function setValFootageToUsed(footage: footage[]): Promise<void> {
  try {
    if (footage?.length > 0) {
      log(`Telling footageService which footage was used...`);
      await axios.post(`http://localhost:3004/footageService/setFootageUsed`, { footage: footage });
    }
  } catch (err) {
    error(`Request to footageService (set to used) failed...`, err);
  }
}

// Utility functions

async function generateCompTitle(): Promise<string> {
  // Current month
  const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  let month = monthNames[new Date().getMonth()].toUpperCase();

  // Version number
  const { version_number } = await prisma.compilation.findUnique({ where: { id: 1 } });
  return `MEME COMPILATION V${version_number} - ${month} ${new Date().getFullYear()}`;
}

async function increaseVersionNumber(): Promise<void> {
  await prisma.compilation.update({
    where: { id: 1 },
    data: { version_number: { increment: 1 } }
  });
}

function log(msg: string, date: boolean = false): void {
  let e = new Error();
  let frame = e.stack.split("\n")[2];
  let functionName = frame.split(" ")[5];
  logger.log(`[${functionName}] ${date ? dateFormat(new Date(), "yyyy-mm-dd@hh:MM:ss") + " " : ""}`).joint().bold().log(msg);
}

function error(msg: string, error: string): void {
  let e = new Error();
  let frame = e.stack.split("\n")[2];
  let functionName = frame.split(" ")[5];
  logger.bold().bgColor("red").log("[ERROR]").joint().log(`[${functionName}] ${dateFormat(new Date(), "yyyy-mm-dd@hh:MM:ss")} `).joint().bold().log(" " + msg);
  logger.log(error);
}
