export const selectors = {
    // General
    file: 'input[type=file]',
    nextButton: '#next-button',
    processing: ':nth-child(8) > .still-processing-scrim',
    // Page one
    pageOne: {
        thumb: 'input[type=file]',
        title: '#title-textarea #textbox',
       
        desc: '#description-container #textbox',
        playlist: '#basics > div:nth-child(7) > div.compact-row.style-scope.ytcp-video-metadata-editor-basics > div:nth-child(1) > ytcp-video-metadata-playlists > ytcp-text-dropdown-trigger > ytcp-dropdown-trigger > div',
        playlistDone: '#dialog > div.action-buttons.style-scope.ytcp-playlist-dialog > ytcp-button.done-button.action-button.style-scope.ytcp-playlist-dialog',
        madeForKids: '[name="VIDEO_MADE_FOR_KIDS_MFK"]',
        notMadeForKids: '[name="VIDEO_MADE_FOR_KIDS_NOT_MFK"]',
        ageArrow: '#audience > ytcp-form-audience > ytcp-audience-picker > button',
        age: '[name="AGE_RESTRICTED]',
        showMore: '#toggle-button > .label',
        promotion: '#has-ppp > div.style-scope.ytcp-checkbox-lit.label',
        automaticChapters: '#details > div > ytcp-video-metadata-editor-advanced > div:nth-child(2) > ytcp-form-checkbox',
        tagBox: '.ytcp-video-metadata-editor-advanced > #chip-bar > .chip-and-bar > #text-input',
    },
    // Page two
    pageTwo:{
        endcard: {
            importFromVideo: '#import-from-video-button',
            defaultImport: '#endscreens-button',

            //TODO: Rest of endcard
        },
        card: {

            //TODO: Rest of cards
        }
    },
    // Page four
    pageFour: {
        uploadLinkTag: '.value > .video-url-fadeable > .style-scope',
        instantPremiereCheckbox: '#enable-premiere-checkbox',
        submitVideoButton: '#done-button',
        watchUrl: '#watch-url'
    },
    // Creator comment
    creatorComment: {
        commentBox: '#simple-box',
        commentBoxInput: '#placeholder-area',
        commentSubmit: '#submit-button',
        pin : {
            openCommentMenu: '.ytd-comment-renderer > .dropdown-trigger > #button > .ytd-menu-renderer',
            clickPin: ':nth-child(1) > .yt-simple-endpoint > tp-yt-paper-item.style-scope > yt-icon.style-scope',
            confirmPin: '#confirm-button > .yt-simple-endpoint > #button > #text'
        }
    },
    // Login
    login: {
        accountName: '.whsOnd',
        password: '.whsOnd',
        nextBtn: '.VfPpkd-LgbsSe'
    }
}