import { uploadSpecs } from './interface/types';
import { selectors } from './interface/selectors';
import * as Browser from './browser/browser'
import * as fs from 'fs-extra';
import * as types from './interface/types';
import path from 'path';

const prompt = require('prompt-sync')();
let defaultDelayMultiplier = 1;

async function uploadVideo(uploadSpecs: types.uploadSpecs): Promise<void> {
    let studioLink = 'https://studio.youtube.com/channel/' + uploadSpecs.channelId + '/videos/upload?d=ud'

    await Browser.init();
    await Browser.getPage().goto(studioLink);
    await sleep(2000);

    await Browser.getPage().waitForSelector(selectors.file).catch(e => {
        error('Did not manage to upload Video, are you logged in?')
    });
    const elementHandleVideo = await Browser.getPage().$(selectors.file);
    if (!elementHandleVideo) {
        error("Cant upload Video, please check if you are logged in and that the Studio Link is valid!");
    }
    await elementHandleVideo.uploadFile(uploadSpecs.video)
        .catch(err => { error("Error uploading the Video!"); });

    await sleep(5000);
    await pageOne(uploadSpecs.pageOne);
    await pageTwo(uploadSpecs.pageTwo);
    await pageThree(uploadSpecs.pageThree);
    let uploadlink = await pageFour(uploadSpecs.pageFour);
    if (uploadSpecs.creatorComment?.text) {
        await sleep(3000);
        writeCreatorComment(uploadSpecs.creatorComment, uploadlink);
    }
    Browser.close();
}

async function pageOne(specs: any): Promise<void> {

    // Upload
    const elementHandleThumb = await Browser.getPage().$(selectors.pageOne.thumb);
    await elementHandleThumb.uploadFile(specs.thumb).catch(err => { 
        console.log(err)
    })
    await sleep(2000);

    // Setting title
    await Browser.getPage().$eval(selectors.pageOne.title,
        (el: { innerHTML: any; }, value: string) => { el.innerHTML = value }, specs.title);
    await Browser.getPage().type(selectors.pageOne.title, " ");
    await sleep(1000);

    // Setting description
    await Browser.getPage().$eval(selectors.pageOne.desc,
        (el: { innerHTML: any; }, value: string) => { el.innerHTML = value }, specs.description ?? '')
    await Browser.getPage().type(selectors.pageOne.desc, " ");
    await sleep(1000);

    // Add to Playlist
    if (specs.playlists) {
        await Browser.getPage().click(selectors.pageOne.playlist)
        await sleep(5000);
        for (let i = 0; i < specs.playlists?.length; i++) {
            let c = 0;
            while (true) {
                let error = false;
                await Browser.getPage().$eval('#checkbox-label-' + c + ' > span',
                    (el: { textContent: any; click: () => any; }, value: string) => { el.textContent == value ? el.click() : el }, specs.playlists[i])
                    .catch(err => { error = true })
                c++; // <--- xDDD
                if (error) {
                    break;
                }
            }
            sleep(500);
        }
        sleep(2000);
        await Browser.getPage().click(selectors.pageOne.playlistDone).catch(e => { });
        sleep(1000);
    }

    // Select made for Kids
    if (specs.madeForKids) {
        await Browser.getPage().click(selectors.pageOne.madeForKids);
    } else {
        await Browser.getPage().click(selectors.pageOne.notMadeForKids);
    }
    await sleep(2000);

    // Restrict to adult only
    if (specs.restrictForAdultsOnly) {
        await Browser.getPage().click(selectors.pageOne.ageArrow);
        await Browser.getPage().click(selectors.pageOne.age);
        await sleep(1000);
    }

    // Show more
    await Browser.getPage().click(selectors.pageOne.showMore);
    await sleep(1000);

    // Set paid promotion
    if (specs.paidPromotion) {
        await Browser.getPage().click(selectors.pageOne.promotion);
        await sleep(1000);
    }

    // Allow Automatic Chapters
    if (!specs.allowAutomaticChapters && typeof specs.allowAutomaticChapters !== 'undefined') {
        await Browser.getPage().click(selectors.pageOne.automaticChapters);
        await sleep(1000);
    }

    // Set Tags
    if(specs.tags){

   
    let tagstring = '';
    specs.tags?.forEach(tag => {
        tagstring += tag + ','
    })
    
    await Browser.getPage().type(selectors.pageOne.tagBox, tagstring).catch(e => console.log(e));
    await sleep(1000);
}
    // Set Language

    // Set Caption

    // Set Record Date

    // Set license

    // Allow Embedding

    // Notify Subs

    // Allow Sampling

    // Set category

    // Set comment visibility

    // Sort comments by

    // Show like/dislike ratio

    // Clicking to next page (video elements)
    await Browser.getPage().$$eval(selectors.nextButton, (elements) =>
        elements[0].click()
    );
    await sleep(500);
}

async function pageTwo(specs: any): Promise<void> {

    // if (!specs) {
    //     // Clicking to next page (video elements)
    //     await Browser.getPage().$$eval("#next-button", (elements) =>
    //         elements[0].click()
    //     );
    //     await sleep(1000);
    //     return
    // }
    await Browser.getPage().waitForSelector(
        selectors.processing,
        { hidden: true, timeout: 0 }
    );

    // Subtitles

    // Add Endscreen
    if (specs?.endcard?.enabled) {
        let selector = specs.endcard.import ? selectors.pageTwo.endcard.importFromVideo : selectors.pageTwo.endcard.defaultImport;

        const isDisabled = await Browser.getPage().$eval(selector, (button) => {
            return button.disabled;
        });
        if (!isDisabled) {
            await Browser.getPage().click(selectors.pageTwo.endcard.defaultImport);
            await sleep(1000);

            try {
                if (specs.endcard.import) {
                    await Browser.getPage().waitForSelector(selectors.pageTwo.endcard.importFromVideo);
                    await Browser.getPage().click('#import-endscreen-from-video-button');
                    await Browser.getPage().click('#search-yours')
                    await Browser.getPage().type('#search-yours', specs.endcard.importId)
                    await Browser.getPage().waitForSelector('#dialog > div.content.style-scope.ytcp-dialog > div > div > div > ytcp-entity-card').catch(e => { });
                    await sleep(2000);
                    await Browser.getPage().click('#dialog > div.content.style-scope.ytcp-dialog > div > div > div > ytcp-entity-card');
                } else {
                    await Browser.getPage().click('#cards-row > div:nth-child(' + specs.endcard.type + ') > div.template-preview.style-scope.ytve-endscreen-template-picker')
                }
            } catch (e) { }
            await sleep(3000);
            await Browser.getPage().click('#save-button')
            await sleep(3000);
        }
    }

    // Add Cards
    if (specs?.cards?.enabled) {

        if (specs.cards.type == types.card.PLAYLIST) {
            specs.cards.anyVideo = true;
        }

        await Browser.getPage().click('#cards-button > div');
        await sleep(1000);

        try {
            await Browser.getPage().waitForSelector('.ytve-info-cards-editor-options-panel > :nth-child(' + specs.cards.type + ')');
            await Browser.getPage().click('.ytve-info-cards-editor-options-panel > :nth-child(' + specs.cards.type + ')');
            await sleep(500);

            let selector = specs.cards.anyVideo ? '#search-any' : '#search-yours';
            await Browser.getPage().waitForSelector(selector)
            await Browser.getPage().click(selector)
            await Browser.getPage().type(selector, specs.cards.name);
            //await sleep(1000);
            await Browser.getPage().waitForSelector('.card > #content > .thumbnail')
            await Browser.getPage().click('.card > #content > .thumbnail');

            await Browser.getPage().type('#custom-message > .text-container > #textarea-container > textarea.style-scope', specs.cards.message).catch(err => { });

            await Browser.getPage().type('#teaser-text > .text-container > #textarea-container > textarea.style-scope', specs.cards.teaser).catch(err => { });
        } catch (e) { }
        // TODO add time when card shows

        await Browser.getPage().click('#save-button > .label');
        await sleep(1000);
    }

    // Clicking to next page (video elements)
    await Browser.getPage().$$eval(selectors.nextButton, (elements) =>
        elements[0].click()
    );
    await sleep(500);
}

async function pageThree(specs: any): Promise<void> {
    // Clicking to next page (video elements)
    await Browser.getPage().$$eval(selectors.nextButton, (elements) =>
        elements[0].click()
    );
    await sleep(500);
}

async function pageFour(specs: any): Promise<string> {

    // Set visibility
    await Browser.getPage().click('[name="' + specs.visibility + '"]');
    await sleep(2000);

    // Get video link
    let uploadLink: string = await Browser.getPage().evaluate('document.querySelector("' + selectors.pageFour.uploadLinkTag + '").getAttribute("href")');

    // Set instant premiere
    if (specs.instantPremiere) {
        await Browser.getPage().click(selectors.pageFour.instantPremiereCheckbox);
    }

    // Scheudule

    // Submit video
    await Browser.getPage().click(selectors.pageFour.submitVideoButton);
    await sleep(5000);
    try {
        await Browser.getPage().waitForSelector(selectors.pageFour.watchUrl);
    } catch (e) {

    }

    return uploadLink;
}

async function writeCreatorComment(comment: { text: string, pin?: boolean }, uploadLink: string): Promise<void> {

    // Without changing anything, the browser now crashes if we directly open the link, so we have to reopen the browser
    await Browser.close();
    await Browser.init();

    // Write creator comment
    await Browser.getPage().goto(uploadLink),
        await sleep(2000);

    // Scroll
    await Browser.getPage().evaluate(() => { window.scrollBy(0, window.innerHeight) });

    // Look for comments box while scrolling and click it
    await Browser.getPage().waitForSelector(selectors.creatorComment.commentBox, { visible: true });
    await Browser.getPage().click(selectors.creatorComment.commentBoxInput);

    // Write comment
    await Browser.getPage().type(selectors.creatorComment.commentBoxInput, comment.text, { delay: 100 });

    // Submit comment
    await Browser.getPage().click(selectors.creatorComment.commentSubmit);

    await sleep(2000); // need to wait for comment to be posted, can't be bothered to find selector

    if (comment.pin) {
        // Pin first comment (our written comment)
        await Browser.getPage().click(selectors.creatorComment.pin.openCommentMenu);
        await Browser.getPage().click(selectors.creatorComment.pin.clickPin);

        // Confirm pin
        await Browser.getPage().waitForSelector(selectors.creatorComment.pin.confirmPin, { visible: true });
        await Browser.getPage().click(selectors.creatorComment.pin.confirmPin);
    }
    Browser.close();
}

async function login(account: { name: string, password: string, twofa?: boolean }) {

    if (!account?.name) {
        error('Please provide an E-Mail or Phonenumber to login to your Google-Account');
    }

    if (!account?.password) {
        error('Please provide a Password to login to your Google-Account');
    }

    await Browser.init();
    await Browser.getPage().goto("https://accounts.google.com/servicelogin");

    await sleep(2000);
    await Browser.getPage().type(selectors.login.accountName, account.name).catch(err => {
        Browser.close();
        throw Error("You are already logged in!");
    });

    await Browser.getPage().$$eval(selectors.login.nextBtn, (elements) =>
        elements[0].click()
    );
    await sleep(2000);

    await Browser.getPage().type(selectors.login.password, account.password);
    await Browser.getPage().$$eval(selectors.login.nextBtn, (elements) => elements[0].click());

    if (account.twofa) {
        prompt('Please accept the login and then press ENTER');
        await sleep(3000);
    } else {
        await sleep(10000);
    }

    Browser.close();
}

// Parsing the user given specs
function parseUploadSpecs(uploadSpecs: uploadSpecs): uploadSpecs {
    let errors = '';

    if (!uploadSpecs.channelId) {
        errors += 'Error: channelId missing\n';
    }

    if (!fs.existsSync(uploadSpecs.video)) {
        errors += 'Error: video file missing\n';
    }

    if (uploadSpecs.pageTwo?.endcard?.enabled && !uploadSpecs.pageTwo?.endcard?.import && !Object.values(types.endcard).includes(uploadSpecs.pageTwo.endcard.type)) {
        errors += 'TypeError: endcard must be of type endcard\n';
    }

    if (uploadSpecs.pageTwo?.cards?.enabled && !Object.values(types.card).includes(uploadSpecs.pageTwo.cards.type)) {
        errors += 'TypeError: card must be of type cards\n';
    }

    if (!Object.values(types.visibility).includes(uploadSpecs.pageFour.visibility)) {
        errors += 'TypeError: visibility must be of type visibility\n';
    }

    if (errors.length) {
        error(errors);
    }

    return uploadSpecs;
}

function error(msg: string): void {
    let e = new Error;
    console.error(msg, e);
    Browser.close();
    process.exit(1);
}

function sleep(ms: number): Promise<void> {
    ms * defaultDelayMultiplier;
    return new Promise(resolve => setTimeout(resolve, ms))
}

async function reset(): Promise<void> {
    await Browser.resetBrowser();
}

async function upload(uploadSpecs: uploadSpecs): Promise<void> {
    let parsedSpecs = parseUploadSpecs(uploadSpecs);
    await uploadVideo(parsedSpecs);
}

function setHead(bool: boolean): void {
    Browser.toggleHeadless(!bool);
}

function setDefaultDelayMultiplier(multiplier: number): void {
    this.defaultDelayMultiplier = multiplier;
}

// function setExecutable(executable): void {
//     Browser.setExecutable(executable);
// }

export {
    upload,
    login,
    reset,
    setHead,
    types,
    setDefaultDelayMultiplier,
    // setExecutable
}
