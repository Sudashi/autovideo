/**
 * @author gitlab.com/sk_in_the_house
 */

import fs from 'fs-extra';
import spawn from 'await-spawn';

// Don't use spaces, linux will escape them: " " -> "\ "
const outputFileName = 'tempvideo';

/**
 * Blurs videos, converts them into 1280x720 and combines them into one single video
 * 
 * @param {String} workingDir - Specifies the video directory in which to edit (no slash at the end!)
 */
export default async function editFootage(workingDir: string): Promise<string> {

    // Edit individual videos
    await blurVideoBackgrounds(workingDir);
    await resizeVideos(workingDir);
    await checkAndFixAudioStreams(workingDir);

    // Combine clips
    return await combineClips(workingDir);

}

/**
 * Converts portraits to landscape, it will only blur parts of the video that are outside of the original video bounding box, thus working for both 16:9
 * and 16:9 videos. Should it fail to blur a video, then it will simply remain unblurred.
 * 
 * @param {String} sourceDir - Specifies the directory that contains the videos to be blurred (no slash at the end!)
 */
async function blurVideoBackgrounds(sourceDir: string): Promise<void> {
    console.info("Blurring videos...");

    // Blur videos
    const videos = await fs.readdir(sourceDir);
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i];
        try {
            await spawn('ffmpeg', [
                '-i', `${sourceDir}/${video}`, '-vsync', '-1', '-vf',
                'split[original][copy];[copy]scale=ih*16/9:-1,crop=h=iw*9/16,gblur=sigma=20[blurred];[blurred][original]overlay=(main_w-overlay_w)/2:(main_h-overlay_h)/2',
                `${sourceDir}/blurred_${video}`
            ]);
            fs.unlinkSync(`${sourceDir}/${video}`);
            console.info(`Blurred ${video}`);
        } catch (error) {
            console.error(`There was an error blurring ${video}!`, error.stderr._bufs.toString());
        }
    }
    console.info("Blurred all videos!");
}

/**
 * Converts videos to 1280x720 - Should this fail, then the video is deleted in the sourceDir
 * 
 * @param {String} sourceDir - Specifies the directory that contains the videos to be resized (no slash at the end!)
 */
async function resizeVideos(sourceDir: string): Promise<void> {
    console.info("Resizing videos...");

    // Resize videos
    const videos = await fs.readdir(sourceDir);
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i];
        let outputPath = `${sourceDir}/resized_${video}`;
        try {
            await spawn('ffmpeg', [
                '-i', `${sourceDir}/${video}`, '-vsync', '-1', '-vf',
                'scale=1280:720,setsar=1:1', '-c:v', 'libx264', '-c:a', 'copy',
                outputPath
            ]);
            console.info(`Resized ${video}`);
        } catch (error) {
            console.error(`There was an error resizing ${video}!`, error.stderr._bufs.toString());
            // Unlink failed shell of output video 
            if (fs.existsSync(outputPath)) {
                fs.unlinkSync(outputPath);
            }
        } finally {
            fs.unlinkSync(`${sourceDir}/${video}`);
        }
    }
    console.info("Resized all videos!");
}

/**
 * Adds silent audio to videos with no audio (to prevent combineClips from crashing)
 * 
 * @param {String} sourceDir - Specifies the directory that contains the videos to be checked
 */
async function checkAndFixAudioStreams(sourceDir: string): Promise<void> {
    console.info("Checking audio streams of videos...");

    // Resize videos
    const videos = await fs.readdir(sourceDir);
    for (let i = 0; i < videos.length; i++) {
        const video = videos[i];
        let outputPath = `${sourceDir}/audio_fixed_${video}`;
        try {
            await spawn('ffmpeg', [
                '-f', 'lavfi', '-i', 'anullsrc=channel_layout=stereo:sample_rate=44100',
                '-i', `${sourceDir}/${video}`, '-c:v', 'copy', '-c:a', 'aac', '-shortest',
                outputPath
            ]);
            console.info(`Checked/fixed audio for ${video}`);
        } catch (error) {
            console.error(`There was an error checking/fixing the audio streams for ${video}!`, error.stderr._bufs.toString());
            // Unlink failed shell of output video 
            if (fs.existsSync(outputPath)) {
                fs.unlinkSync(outputPath);
            }
        } finally {
            fs.unlinkSync(`${sourceDir}/${video}`);
        }
    }
    console.info("Checked/fixed audio streams for all videos!");
}

/**
 * Combines videos in the given directory into one single video
 * 
 * @param {String} sourceDir - Specifies the directory that contains the videos to be combined (no slash at the end!)
 */
async function combineClips(sourceDir: string): Promise<string> {

    // Create concat command (based on concat video filter from https://stackoverflow.com/a/11175851/10233170)
    // We use the first method, because this re-encodes all videos and ensures that their encodings are the same, thus preventing out-of-sync issues
    console.log("Combining clips...");
    const videos = await fs.readdir(sourceDir);

    // Inputs
    let inputs = [];
    for (let i = 0; i < videos.length; i++) {
        inputs.push('-i');
        inputs.push(`${sourceDir}/${videos[i]}`);
    }

    // Streams
    let streams = [];
    for (let i = 0; i < videos.length; i++) {
        streams.push(`[${i}:v]`);
        streams.push(`[${i}:a]`);
    }

    // Concat videos to mkv (straight to mp4 doesn't work)
    try {
        await spawn('ffmpeg', [
            ...inputs,
            '-vcodec', 'libx264', '-crf', '30', '-preset', 'ultrafast', // makes bot go faster UwU
            '-filter_complex', `${streams.join(' ')} concat=n=${videos.length}:v=1:a=1 [v] [a]`,
            '-map', '[v]', '-map', '[a]',
            `${sourceDir}/combined.mkv`
        ]);
        console.log("Combined clips to mkv!");
    } catch (error) {
        console.error(`There was an error combining the videos!`, error.stderr._bufs.toString());
    }

    // Convert mkv to mp4
    try {
        console.log("Converting mkv to mp4...");
        await spawn('ffmpeg', ['-i', `${sourceDir}/combined.mkv`, '-c', 'copy', '-c:a', 'aac', `${sourceDir}/${outputFileName}.mp4`]);
        console.log("Successfully converted!");
    } catch (error) {
        console.error(`There was an error converting the mkv video!`, error.stderr._bufs.toString());
    }

    // Delete source clips and mkv
    videos.forEach(sourceVideo => {
        fs.unlinkSync(`${sourceDir}/${sourceVideo}`);
    });
    try {
        fs.unlinkSync(`${sourceDir}/combined.mkv`);
    } catch (error) {
        // This is happens sometimes, no problemo
    }
    console.log("Deleted source videos and cleaned up mkv!");

    console.info("Combining process has been completed!");
    return `${sourceDir}/${outputFileName}.mp4`;
}