import fs from 'fs-extra';
import path from 'path';
import { requestVideo, setUsedFootage, insertValidatedFootage, sleep } from './utility';
import express from 'express'
const app = express();
import http from 'http'
import { rating, stream } from './types';
import publicIp from 'public-ip';
let IP;

// Create http server to host video stream
const server = http.createServer(app);
const io = require('socket.io')(server, {
  cors: {
    origin: true,
    methods: ["GET", "POST"]
  }
});

// Default route for uptime pinging
app.get("/filterchanserver", (req, res) => {
  res.status(200).send("FilterchanServer online ✅");
});

// Client connection
io.on('connection', client => {

  // Create "videos" directory if it doesnt exist yet
  let directory = path.join(__dirname, "../videos");
  if (!fs.existsSync(directory)) {
    fs.mkdirSync(directory);
    console.log("Created videos directory!");
  }

  // Create user directory for his cached videos
  directory = path.join(directory + "/" + client.id);
  fs.mkdir(directory, (err) => {
    if (err) { return console.error(err); }
    console.log(`Created user directory ${directory}!`);
  });

  // Client requests video, we download it and send a message to the client when it's ready
  client.on('requestVideo', async () => {

    let stream: stream = await requestVideo(client.id);

    // Send a different message if the download fails
    if (!stream) {
      client.emit('requestFailed');
      return;
    }
    stream.url = `http://${IP}:3011/stream/${client.id}/${stream.videoName}`;

    await sleep(1000); // wait a second or the write process isnt finished yet completely and it crashes
    client.emit('streamReady', stream);
  });

  // please rename footageObj to footage at some point
  // Client rates the video and sends us a message
  client.on('rateVideo', async (rating: rating) => {

    // Delete video
    fs.rmSync(directory + '/' + rating.stream.videoName, { force: true });

    // Notify service
    setUsedFootage(rating.stream.service, [rating.stream.footage]);

    if (rating.useIt) {
      // Send to database
      insertValidatedFootage(rating.stream.footage)
    }

  });

  // Client closes window, we delete his folder
  client.on('disconnect', () => {
    fs.rmSync(directory, { recursive: true, force: true });
    console.log(`Directory ${directory} deleted successfully!`)
  });

});

// Use the http server to host the video stream
app.get('/stream/:id/:videoName', (req, res) => {

  let videoName = req.params.videoName;
  let clientId = req.params.id;

  const vidpath = path.join(__dirname, `../videos/${clientId}/${videoName}`)

  // forbidden sorcery which will bring gods wrath upon us
  const stat = fs.statSync(vidpath)
  const fileSize = stat.size
  const range = req.headers.range
  if (range) {
    const parts = range.replace(/bytes=/, "").split("-")
    const start = parseInt(parts[0], 10)
    const end = parts[1]
      ? parseInt(parts[1], 10)
      : fileSize - 1
    const chunksize = (end - start) + 1
    const file = fs.createReadStream(vidpath, { start, end });
    const head = {
      'Content-Range': `bytes ${start}-${end}/${fileSize}`,
      'Accept-Ranges': 'bytes',
      'Content-Length': chunksize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(206, head);
    file.pipe(res);

  } else {
    const head = {
      'Content-Length': fileSize,
      'Content-Type': 'video/mp4',
    }
    res.writeHead(200, head)
    fs.createReadStream(vidpath).pipe(res);

  }
});

(async () => {
  IP = await publicIp.v4() ?? '130.61.130.159';
  server.listen(3011, () => {
    console.log('listening on *:3011');
  });
})();



