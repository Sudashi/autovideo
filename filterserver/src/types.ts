interface footage {
    id?: number;
    url: string;
    title: string;
    timestamp: Date;
    duration: number;
    size: number;
    used: boolean;
}

enum services {
    DISCORD_SERVICE = "discordService",
    REDDIT_SERVICE = "redditService",
    TIKTOK_SERVICE = "tiktokService"
}

const servicePorts = {
    "discordService": 3001,
    "redditService": 3002,
    "tiktokService": 3003
}

interface stream {
    url?: string,
    videoName: string,
    footage: footage,
    service: services,
}

interface rating {
    stream: stream,
    useIt: boolean,
}


export { footage, services, servicePorts, stream, rating };