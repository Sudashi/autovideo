# Entwurf - System & Sofware

addThumbnail(blob)::void -> insert imgobj
validate image (avoid corrupt blob)

getThumbnail()::img,id -> request random imgobj:
random unused img, if no unused left, return random used img + some sort of identifier

setUsedThumbnail(id)::void -> update imgobj -> used -> true

getServiceStatus()::infoObj -> x/y used thumbs, etc.