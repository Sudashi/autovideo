const expect = require("chai").expect;
const request = require("request");
const thumbnailservice = require("../index");
const Database = require("../knexUtility");
const fs = require('fs-extra')
const path = require("path");
require('dotenv').config();


// Refuse to run in production
if (process.env.PRODUCTION == false) {
    return;
}

// Test
describe("Thumbnail Service", function () {

    before(async function () {
        // Runs before all tests, deletes db and runs the latest migration
        try {
            fs.unlinkSync(path.join(`${__dirname}/../database/dev.database`), err => callback(err))
        } catch (e) {
            console.log('Could not delete, aborting...', e)
            return;
         }
        await Database.migrateLatest();
    });

    /**
     * Tests the getThumbnail route for statusCode 200 and that the response contains an
     * object with the fields being all null
     */
    describe("getThumbnail - empty DB", function () {

        let url = "http://localhost:3000/getThumbnail";

        it("Return status code 200", function (done) {
            request(url, function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it("Return null for base64 image string", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.sourceUrl).to.be.equal(null);
                done();
            });
        });

        it("Return null for source url", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.file).to.be.equal(null)
                done();
            });
        });

        it("Return 0 for the id", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.id).to.be.equal(0)
                done();
            });
        });

    });

    /**
     * Tests the addThumbnail route for statusCode 200 and that the response is an array with exactly one id
     */
    describe("addThumbnails", function () {

        let url = 'http://localhost:3000/addThumbnails'; 

        it("Return status code 200", function (done) {
            request.post(
                url,
                { json: [{ file: 'img', sourceUrl: "url", used: false } ]},
                function (error, response, body) {
                    expect(response.statusCode).to.equal(200);
                    done();
                }
            );
        });

        it("Returns success", function (done) {
            request.post(
                url,
                { json: [{ file: 'img', sourceUrl: "url", used: false }] },
                function (error, response, body) {
                    expect(body.result).to.be.a("array");
                    done();
                }
            );
        });

    });

    /**
     * Tests the getThumbnail route for statusCode 200 and that the response is not undefined
     */
    describe("getThumbnail", function () {

        let url = "http://localhost:3000/getThumbnail";

        it("Return status code 200", function (done) {
            request(url, function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it("Return a base64 image string", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.sourceUrl).to.not.equal(undefined);
                done();
            });
        });

        it("Return a source url", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.file).to.not.equal(undefined)
                done();
            });
        });

        it("Return an id", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data.id).to.not.equal(undefined)
                done();
            });
        });

    });

    /**
     * Tests the setThumbnailUsed route for statusCode 200 and that the response is not undefined
     */
    describe("setThumbnailUsed", function () {

        let url = "http://localhost:3000/setThumbnailUsed";

        it("Return status code 200", function (done) {
            request.post(
                url,
                { json: { id: 1 } },
                function (error, response, body) {
                    expect(response.statusCode).to.equal(200);
                    done();
                }
            );
        });

        it("Return the number of updated objects", function (done) {
            request.post(
                url,
                { json: { id: 1 } },
                function (error, response, body) {
                    expect(body.result).to.be.equal(1);
                    done();
                }
            );
        });

    });

    /**
     * Tests the getServiceStatus route for statusCode 200 and that the response is not undefined
     */
    describe("getServiceStatus", function () {

        let url = "http://localhost:3000/getServiceStatus";

        it("Return status code 200", function (done) {
            request(url, function (error, response, body) {
                expect(response.statusCode).to.equal(200);
                done();
            });
        });

        it("Return an object that is not undefined", function (done) {
            request(url, function (error, response, body) {
                let data = JSON.parse(body);
                expect(data).to.not.equal(undefined)
                done();
            });
        });

    });

});
