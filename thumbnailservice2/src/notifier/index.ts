import axios from 'axios';
require('dotenv').config();

export async function notifyUs(message) {
    await axios.get('https://maker.ifttt.com/trigger/notify_us_on_bot_upload/with/key/dfI9OfM5roZ_0s0GuF_euv?value1=' + message).catch(error => { console.error("Couldn't send push notifications to us!", error) });
    console.info("Sent notification!");
}

// PS: I did not write this, Simon
export function notifyDiscordServer(message) {

   
    const AVATAR = process.env.AVATAR || 'https://cdn.discordapp.com/attachments/408564133319933962/864971557814534152/SPOILER_source_Senpai.png';
    let hookUrl = process.env.PRODUCTION === "true" ? process.env.PRODUCTION_HOOK : process.env.PRODUCTION_HOOK;

    // if .env is missing just use productionhook, i bet nothing bad will happen because of this *ever*
    if (!hookUrl) {
        hookUrl = 'https://canary.discord.com/api/webhooks/865142217765290014/-4TdggZEazSLxVkqmq5biqyJOaVNTCCz3aylJkVxAI4GEZkJnB3vqDtu7hIYyQiZ2Xq2'
    }

    axios({
        method: 'post',
        url: hookUrl,
        data: {
            "username": "source Senpai",
            "content": message,
            "avatar_url": AVATAR,
        }
    })
}

let hookUrl = process.env.PRODUCTION === "true" ? process.env.PRODUCTION_HOOK : process.env.DEV_HOOK;
// console.log(hookUrl);
