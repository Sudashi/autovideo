import { PrismaClient } from '@prisma/client'
import createSubscriber from 'pg-listen';
import dotenv from 'dotenv';
import path from 'path';
import Cron from 'croner';
import axios from 'axios';
import express from 'express';

// Postgres notification listener

dotenv.config({ path: path.join(`${__dirname}/../.env`) });
const subscriber = createSubscriber({ connectionString: process.env.DATABASE_URL });
const channelName = 'supervisor';

subscriber.notifications.on(channelName, (payload) => {
    // Triggered when INSERT, UPDATE or DELETE happens in config table of supervisor database
    console.log('Received config update, updating cron jobs...');
    updateCron();
});

subscriber.events.on("error", (error) => {
    console.error("Fatal database connection error:", error);
    process.exit(1);
});

process.on("exit", () => {
    subscriber.close();
});

async function initListener() {
    await subscriber.connect();
    await subscriber.listenTo(channelName);
}

// Cron

const prisma = new PrismaClient();
let tasks = [];

// Deletes all old cron tasks and sets new ones
async function updateCron() {

    // Destroy all old tasks
    tasks.forEach(task => task.stop());
    tasks = [];

    // Init new tasks (schedule -> { time: "16:00:00", weekdays: "1111110", duration: 1 })
    let schedules = await prisma.config.findMany();
    schedules.forEach(schedule => {

        console.log("Creating task for", schedule)
        let { time, weekdays, duration } = schedule;

        // Create cron expression
        let baseCronExpression = `${time.substring(3, 5)} ${time.substring(0, 2)} * *`;

        // Starting at 1 because of cron syntax -> see https://crontab.guru/
        for (let i = 0; i < weekdays.length; i++) {
            const weekday = weekdays[i];

            if (weekday == "1") {

                let cronday = i + 1;
                if (cronday == 7) {
                    cronday = 0;
                }

                let task = Cron(`${baseCronExpression} ${cronday}`, { timezone: "Europe/Vienna" }, () => {
                    requestVideoUpload(duration);
                });
                tasks.push(task);
            }
        }
    });

}

const requestVideoUpload = (duration: number): void => {
    console.log('Sending request to compiler!');
    axios.get(`http://localhost:3021/compiler/uploadVideo/${duration}`);
}

// Init

initListener();
updateCron();

// Small express server for status dashboard (unrelated to the rest of the code)

const PORT = 3020;
const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/supervisor", async (req, res) => {
    let schedules = await prisma.config.findMany();
    res.status(200).json({ status: "Supervisor online ✅", schedules });
});

app.listen(PORT, () => console.log(`Supervisor running on port ${PORT}`));