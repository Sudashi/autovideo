export interface footage {
    id?: number;
    url: string,
    title: string;
    timestamp: Date;
    size: number;
    duration: number;
}

enum services {
    DISCORD_SERVICE = "discordService",
    REDDIT_SERVICE = "redditService",
    TIKTOK_SERVICE = "tiktokService"
}

export interface stream {
    url: string,
    videoName: string,
    footageObj: footage,
    service: services
}