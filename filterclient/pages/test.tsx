import styles from '../styles/Home.module.css'
import { useEffect, useRef } from 'react';
import useState from 'react-usestateref';
import 'react-toastify/dist/ReactToastify.css';
import Div100vh from 'react-div-100vh'

import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/effect-cards"

// import Swiper core and required modules
import SwiperCore, { EffectCards } from 'swiper';
import { stream } from '../types/types';

// install Swiper modules
SwiperCore.use([EffectCards]);

let changeCount = 0;
let originalIndex: any = null;

function Home() {

    /** once the active index has changed and the old video isnt visible anymore
     * set the cachedstream at the index of the old slide to null
     * then once streamReady event is fired, look for array values in cachedstreams will null
     * and replace them with the new loaded content, voila, loading in the infite slider should work now **/

    const videoRef = useRef<any>(null);
    const [swiper, setSwiper, swiperRef] = useState<any>(null);
    const [inverted, setInverted] = useState<any>(false);
    const [cachedStreams, setCachedStreams, streamsRef] = useState<string[]>([
        // "https://cdn.discordapp.com/attachments/762458221641596938/913170852600746004/video0_-_2021-04-18T064329.048-1-1-1.mp4",
        // "https://cdn.discordapp.com/attachments/863695602351996939/913652104432877618/super_idol_Oct_20_2021_1.15.52_PM.mp4"
    ]);

    return (
        <Div100vh className={styles.wrapper}>
            <Swiper onSwiper={setSwiper} effect={'cards'} grabCursor={true} className={styles.swiper} loop={true} runCallbacksOnInit={false} shortSwipes={false}

                // Noen of this here worked.... ffs... i am unable to reliably detect when the user
                // actually went to the next slide and if so, what direction he went

                // https://swiperjs.com/swiper-api#methods-and-properties

            >
                <SwiperSlide className={styles.slide} key={cachedStreams[0]}>
                    <video controls className={styles.video} ref={videoRef}>
                        <source src={cachedStreams[0]} />
                        Your browser doesn't support HTML-5 video
                    </video>
                </SwiperSlide>
                <SwiperSlide className={styles.slide} key={cachedStreams[1]}>
                    <video controls className={styles.video} ref={videoRef}>
                        <source src={cachedStreams[1]} />
                        Your browser doesn't support HTML-5 video
                    </video>
                </SwiperSlide>
            </Swiper>
        </Div100vh >
    )
}

export default Home
