import styles from '../styles/Home.module.css'
import { io } from "socket.io-client";
import { useEffect, useRef } from 'react';
import { stream } from '../types/types';
import useState from 'react-usestateref';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Div100vh from 'react-div-100vh';
import axios from 'axios';

// TODO
// normalize audio
// on desktop left and right

// import { Swiper, SwiperSlide } from "swiper/react";

// // Import Swiper styles
// import "swiper/css";
// import "swiper/css/effect-cards"

// // import Swiper core and required modules
// import SwiperCore, { EffectCards } from 'swiper';

// install Swiper modules
// SwiperCore.use([EffectCards]);

function Home() {

  const [cachedStreams, setCachedStreams, streamsRef] = useState<stream[]>([]);
  const [socket, setSocket, socketRef] = useState<any>(null);
  let failedRequests = 0;

  const [swiper, setSwiper, swiperRef] = useState<any>(null);
  const videoRef = useRef<any>(null);

  // Helper functions

  const useIt = (useIt: boolean) => {

    // If there are less than 2 videos cached, dont allow rating
    // if (cachedStreams.length < 1) {
    //   console.log("There is no video");
    //   return;
    // }

    socket.emit("rateVideo", { stream: streamsRef.current[0], useIt });

    // Remove current video
    setCachedStreams(streamsRef.current.filter((stream, i) => i !== 0));

    // Request new video
    console.log("Sending new Video Request!");
    requestVideo(2);
  }

  const requestVideo = async (amount: number) => {
    console.log(`Requesting ${amount} new video${amount > 1 ? "s" : ""}!`);
    for (let i = 0; i < amount; i++) {
      socket.emit('requestVideo');
      await sleep(2000); // youtube-dl download on the server fail otherwhise occasionally...
    }
  }

  const sleep = (ms: any) => {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  // Setup
  useEffect(() => {

    async function initSocket() {
      const res: any = await axios.get("/api/serverInfo");
      const ip = res.ip;
      setSocket(io(`http://${ip ?? '130.61.130.159'}:3011`));
    }

    initSocket();

  }, []);

  useEffect(() => {
    socket?.on("connect", () => {
      console.log("Connected!");
      // Request videos to be cached
      requestVideo(2)
    });

    socket?.on("streamReady", (stream: stream) => {
      console.log("Video is ready!");
      failedRequests = 0;
      setCachedStreams([...streamsRef.current, stream]);
    });

    socket?.on("requestFailed", () => {
      failedRequests++;
      if (failedRequests <= 5) {
        requestVideo(1);
        console.log("Video request failed! Trying again...");
      } else {
        console.log("You are getting too many errors, stopping requests!");
      }
    });

    socket?.on("disconnect", () => {
      console.log("Disconnected!");
    });

  }, [socket]);

  // useEffect(() => {
  //   swiperRef.current.slideTo(1);
  // }, [swiper])

  // Video function

  const toggleVideo = () => {
    if (videoRef.current.paused) {
      videoRef.current.play();
    } else {
      videoRef.current.pause();
    }
  }

  // Card interactions

  let swipeActive = false;
  let useVideo: any = null;

  return (

    <Div100vh className={styles.wrapper}>

      {/* <Swiper onSwiper={setSwiper} effect={'cards'} grabCursor={true} className={styles.swiper}
        onActiveIndexChange={(swiperCore) => {
          const { activeIndex, previousIndex } = swiperCore;

          if (!swipeActive) {
            swipeActive = true;
          } else {
            return;
          }

          if (activeIndex > previousIndex) {
            // Left
            useVideo = false;
          } else {
            // Right
            useVideo = true;
          }
        }}

        onTouchEnd={(event) => {
          // Release swipe
          swipeActive = false;
          // Load new videos
          if (useVideo !== null) {
            console.log("Rate", useVideo)
            useIt(useVideo);

            // Swipe left/right
            if (useVideo) {
              // Left -> Insert one to the left, remove on the right

            } else {
              // Right -> Insert one to the right, remove on the left

            }

            useVideo = null;
          }
        }}
        onTap={(event) => {
          toggleVideo();
        }}
      >
        {cachedStreams.map(function (item, i) {
          return (
            <SwiperSlide className={styles.slide} key={i}>
              <video controls key={cachedStreams[i]?.url} className={styles.video} ref={videoRef}>
                <source src={cachedStreams[i]?.url} />
                <source src="https://cdn.discordapp.com/attachments/762458221641596938/913170852600746004/video0_-_2021-04-18T064329.048-1-1-1.mp4" />
                Your browser doesn't support HTML-5 video
              </video>
            </SwiperSlide>
          )
        })}
      </Swiper> */}

      <div key={cachedStreams[0]?.url} className={styles.videoWrapper} onClick={(event) => { toggleVideo(); }}>
        <video autoPlay controls key={cachedStreams[0]?.url} className={styles.video} ref={videoRef}>
          {/* <source src="https://cdn.discordapp.com/attachments/863695602351996939/913198247533760542/VID-20211124-WA0003.mp4" />
          <source src="https://cdn.discordapp.com/attachments/863695602351996939/913199575702069288/nintendo.mp4" />
          <source src="https://cdn.discordapp.com/attachments/863695602351996939/913199597311111198/real_vibe-1.mp4" /> */}
          <source src={cachedStreams[0]?.url} />
          Your browser doesn't support HTML-5 video
        </video>
      </div>

      <div className={styles.btnWrapper}>
        {cachedStreams.length != 0 ?
          <>
            <button className={`${styles.button} ${styles.no}`} onClick={() => { useIt(false) }}>🗑️ Trash</button>
            <button className={`${styles.button} ${styles.yes}`} onClick={() => { useIt(true) }}>✨ Gold</button>
          </>
          :
          <p className={styles.loading}>Loading new videos </p>
        }
      </div>

    </Div100vh >
  )
}

export default Home
