# Welcome!

This is the main repo for AutoVideo. This application scrapes social media (Reddit, Discord, TikTok) for content, creates a video, finds a thumbnail and uploads it to YouTube on a fixed schedule. All of that, fully automated!

## Architecture

![alt text](architecture.png "Architecture")


## Installation

Clone the repo and then pull the submodules.

```bash
git clone git@gitlab.com:eztube/composer.git
git submodule update --init --recursive
git pull --recurse-submodules
```

### Windows
Make sure you have [docker](https://github.com/docker) installed, this should come with the docker compose command.

### Linux 

Install docker and docker-compose. On arm64 systems it might not work with apt-get and you need to install it via the repo directly.

eg.

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/v2.1.1/docker-compose-linux-aarch64" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
```

## Usage

Use 
```bash
docker-compose up -d
```
to start up all the services.

If one one of the images needs to be rebuild you need to delete the container first and then remove the image and then start it up again.
```bash
docker rm <containerid>
docker rmi <imageid>
docker-compose up <service>
```

Should you want to reset everything, then use the following commands to delete all volumes, all images and rebuild everything:
```
docker compose down -v
for /F %i in ('docker images -a -q') do docker rmi -f %i
docker compose up
```

With the first start-up default values will get put into the database and they can be edited via the not yet existing dashboard or with adminer on port 8080. Before the the autocompiler can upload a video you need to login with your Google-Account first.

## Submodule changes

You need to update the submoudle repo by simply pushing the changes (git add, git commit, git push origin HEAD:main or master).
But then make sure to also push the main repo (compiler) so that the version changes are tracked.

Then just pull on the server with git pull --recurse-submodules

## Editing

Additional submodules can be added with

```bash
git submodules add <clone-link>
```

If you make data changes to the database use
```bash
npx prisma db pull
npx prisma generate
```
in every affected submodule to automatically pull the latest model and generate the latest client(postgres container needs to running and the db string needs to contain localhost instead of <containername>).

On new modules use 
```bash
npx prisma init
```
first.


# Autocompiler

You HAVE to copy the relevant arm64 binary into the autcompiler or it wont run! Arm64 on linux is just a pain...